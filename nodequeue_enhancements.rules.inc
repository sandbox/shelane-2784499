<?php

/**
 * @file
 * Rules for nodequeues.
 */

/**
 * Implements hook_rules_action_info().
 */
function nodequeue_enhancements_rules_action_info() {
  return array(
    "nodequeue_enhancements_action_clear_queue" => array(
      "label" => t("Clear Queue"),
      "group" => t("Nodequeue"),
      "parameter" => array(
        "queue_to_clear" => array(
          "label" => t("Queue to Clear"),
          "type" => "text",
          "options list" => "nodequeue_enhancements_queues",
        ),
      ),
    ),
    "nodequeue_enhancements_action_get_index" => array(
      "label" => t("Get Node Index in Queue"),
      "parameter" => array(
        "queue" => array(
          "label" => t("Queue ID from Load Queue"),
          "type" => "text",
        ),
        "node" => array(
          "label" => t("Node to Check"),
          "type" => "node",
        ),
      ),
      "group" => t("Nodequeue"),
      "provides" => array(
        "index" => array(
          "label" => t("Index of Node"),
          "type" => "integer",
        ),
        "queue" => array(
          "label" => t("Queue ID for other actions"),
          "type" => "text",
        ),
        "node" => array(
          "label" => t("The node used."),
          "type" => "node",
        ),
      ),
    ),
    "nodequeue_enhancements_action_load_queue" => array(
      "label" => t("Load Queue"),
      "parameter" => array(
        "queue" => array(
          "label" => t("Queue to Load"),
          "type" => "text",
          "options list" => "nodequeue_enhancements_queues",
        ),
      ),
      "group" => t("Nodequeue"),
      "provides" => array(
        "queued_nodes" => array(
          "label" => t("Queued Nodes"),
          "type" => "list<node>",
        ),
        "queue" => array(
          "label" => t("Queue ID for other actions"),
          "type" => "text",
        ),
      ),
    ),
  );
}

/**
 * Returns the desired subqueue, or NULL.
 */
function nodequeue_enhancements_get_queue($queue, $task = "") {
  $subq = nodequeue_load_subqueues_by_queue(array($queue));
  if ($subq) {
    return array_shift($subq);
  }
  else {
    $message = "Unable to load queue @queue";
    watchdog("rules", "Unable to load queue @queue @task.", array("@queue" => $queue, "@task" => $task));
    return NULL;
  }
}

/**
 * Returns a list of all nodequeue names/labels.
 */
function nodequeue_enhancements_queues($element, $name = NULL) {
  $queues = nodequeue_load_queues(nodequeue_get_all_qids());

  $options = array();
  foreach ($queues as $qid => $queue) {
    $options[$qid] = $queue->title;
  }

  return $options;
}

/**
 * Implementation of queue clearing.
 */
function nodequeue_enhancements_action_clear_queue($queue_to_clear) {
  $subq = nodequeue_enhancements_get_queue($queue_to_clear, "for clearing");
  if ($subq) {
    nodequeue_queue_clear($subq->sqid);
  }
}

/**
 * Loads the specified queue's contents into the rules context.
 */
function nodequeue_enhancements_action_load_queue($queue) {
  $sq = nodequeue_enhancements_get_queue($queue, "for loading nodes");
  if ($sq) {
    $nodes = nodequeue_load_nodes($sq->sqid, FALSE, 0, 0, FALSE);
    return array("queued_nodes" => $nodes, "queue" => $sq->sqid);
  }
  else {
    return array("queued_nodes" => array(), "queue" => "");
  }
}

/**
 * Get the index of the node in the specified subqueue.
 */
function nodequeue_enhancements_action_get_index($sqid, $node) {
  return array(
    'index' => nodequeue_get_subqueue_position($sqid, $node->nid),
    "node" => $node,
    "queue" => $sqid,
  );
}
